import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:dio/dio.dart';

class FriendListRepo {
  Future<List<FriendListResponseModel>> friendListAsync() async {
    try {
      final dio = Dio();
      dio.options.baseUrl = ApiUrls.baseUrl;

      final response = await dio.get(ApiUrls.friendList);
      List<FriendListResponseModel> friendList = [];
      Map<String, dynamic> myMap = new Map<String, dynamic>.from(response.data);

      for (int i = 0; i < myMap.length; i++) {
        FriendListResponseModel friend =
            FriendListResponseModel.fromJson(myMap['data'][i]);
        friendList.add(friend);
      }
      return friendList;
    } on DioError catch (ex) {
      if (ex.response.statusCode == 400) {
        return Future.error(ex.response.data['error']);
      } else {
        return Future.error(ex);
      }
    } catch (ex) {
      return Future.error(ex);
    }
  }
}
