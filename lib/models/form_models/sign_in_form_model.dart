class SignInFormModel {
  String email;
  String password;

  SignInFormModel({
    this.email,
    this.password,
  });
}
