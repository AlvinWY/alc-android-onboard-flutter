class FriendListResponseModel {
  int id;
  String avatar;
  String firstName;
  String lastName;
  String email;

  FriendListResponseModel(
      {this.id, this.avatar, this.firstName, this.lastName, this.email});

  factory FriendListResponseModel.fromJson(Map<String, dynamic> json) =>
      FriendListResponseModel(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        avatar: json["avatar"],
      );

  @override
  String toString() {
    return '{ ${this.id}, ${this.email} }';
  }
}
