import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:alcodes_on_board_flutter/repository/friend_list_repo.dart';
import 'package:alcodes_on_board_flutter/screens/my_friend_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// TODO Issue on this screen:
/// - Show list of my friends. (DONE)
/// - Click list item go to my friend detail page. (DONE)

class MyFriendListScreen extends StatefulWidget {
  @override
  _MyFriendListScreenState createState() => _MyFriendListScreenState();
}

class _MyFriendListScreenState extends State<MyFriendListScreen> {
  List<FriendListResponseModel> _friendList = [];
  bool _loading = true;

  @override
  void initState() {
    final friendRepo = FriendListRepo();
    friendRepo.friendListAsync().then((friendList) {
      setState(() {
        _friendList = friendList;
        _loading = false;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _loading ? Text("Loading...") : Text('My Friend List'),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          new MyFriendDetailScreen(_friendList[index])));
            },
            child: Card(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 5.0, 0, 5.0),
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage:
                              NetworkImage(_friendList[index].avatar),
                          radius: 50,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 15.0,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${_friendList[index].firstName} ${_friendList[index].lastName}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                          ),
                        ),
                        SizedBox(
                          height: 4.0,
                        ),
                        Text(
                          '${_friendList[index].email}',
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 18.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: _friendList.length,
      ),
    );
  }
}
