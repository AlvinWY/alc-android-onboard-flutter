import 'package:alcodes_on_board_flutter/constants/api_urls.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermScreen extends StatefulWidget {
  @override
  _TermScreenState createState() => _TermScreenState();
}

class _TermScreenState extends State<TermScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Terms of Use'),
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 0),
          child: WebView(
            initialUrl: ApiUrls.termsOfUse,
            navigationDelegate: (NavigationRequest request) {
              return NavigationDecision.prevent;
            },
          ),
        ));
  }
}
