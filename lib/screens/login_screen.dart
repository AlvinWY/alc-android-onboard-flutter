import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/form_models/sign_in_form_model.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen:
/// - Password field should hide the inputs for security purpose. (DONE)
/// - Login button is too small, make it fit the form width. (DONE)
/// - Should show loading when calling API, and hide loading when server responded. (DONE)
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use pupop dialog.

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = SignInFormModel();
  String buttonText = 'Login';

  void changeText(String text) {
    setState(() {
      if (text == null) {
        text = 'Login';
      }
      buttonText = text;
    });
  }

  Future<void> _onSubmitFormAsync() async {
    // Hide keyboard.
    AppFocusHelper.instance.requestUnfocus();

    // Validate form and save inputs to form model.
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      changeText('Loading');

      // Call API.
      var alertDialogStatus = AppAlertDialogStatus.error;
      var alertDialogMessage = '';

      try {
        final repo = AuthRepo();
        final apiResponse = await repo.signInAsync(
          email: _formModel.email,
          password: _formModel.password,
        );

        // Login success, store user credentials into shared preference.
        final sharedPref = await SharedPreferences.getInstance();

        await sharedPref.setString(
            SharedPreferenceKeys.userEmail, _formModel.email);
        await sharedPref.setString(
            SharedPreferenceKeys.userToken, apiResponse.data.token);
        changeText('Login');

        // Navigate to home screen.
        Navigator.of(context)
            .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);

        return;
      } catch (ex) {
        Fimber.e('d;;Error request sign in.', ex: ex);

        alertDialogMessage = '$ex';
      }

      if (alertDialogMessage.isNotEmpty) {
        // Has message, show alert dialog.
        final appAlertDialog = AppAlertDialog();
        changeText('Login');

        appAlertDialog.showAsync(
          context: context,
          status: alertDialogStatus,
          message: alertDialogMessage,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                initialValue: 'eve.holt@reqres.in',
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSaved: (newValue) => _formModel.email = newValue,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                decoration: InputDecoration(
                  hintText: 'Email',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                obscureText: true,
                initialValue: 'cityslicka',
                textInputAction: TextInputAction.done,
                onSaved: (newValue) => _formModel.password = newValue,
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Password',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ElevatedButton(
                    onPressed: _onSubmitFormAsync,
                    child: Text('$buttonText'),
                  ),
                ],
              ),
              SizedBox(
                height: 8.0,
              ),
              Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, AppRouter.term);
                    },
                    child: Text(
                      'Terms of Use',
                      style: TextStyle(
                        color: Colors.lightBlue,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
