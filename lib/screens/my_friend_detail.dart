import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/models/response_models/friend_list_response_model.dart';
import 'package:flutter/material.dart';

class MyFriendDetailScreen extends StatelessWidget {
  final FriendListResponseModel _friend;

  MyFriendDetailScreen(this._friend) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('My Friend - ${_friend.firstName} ${_friend.lastName}')),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: appConst.kDefaultPadding,
            ),
            Center(
              child: CircleAvatar(
                backgroundImage: NetworkImage(_friend.avatar),
                radius: 100,
              ),
            ),
            SizedBox(
              height: appConst.kDefaultPadding,
            ),
            friendInRow(title: "First Name", data: _friend.firstName),
            SizedBox(
              height: appConst.kDefaultPadding,
            ),
            friendInRow(title: "Last Name", data: _friend.lastName),
            SizedBox(
              height: appConst.kDefaultPadding,
            ),
            friendInRow(title: "Email Address", data: _friend.email),
          ],
        ),
      ),
    );
  }

  Container friendInRow({String title, String data}) {
    return Container(
            color: Colors.lightBlue[100],
            margin: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            padding: EdgeInsets.all(13.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  data,
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          );
  }
}
