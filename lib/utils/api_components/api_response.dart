class ApiResponse<T> {
  String message;
  T data;

  ApiResponse({
    this.message = '',
    this.data,
  });

  @override
  String toString() => message;
}
