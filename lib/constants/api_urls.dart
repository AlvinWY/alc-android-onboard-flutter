class ApiUrls {
  static final String baseUrl = 'https://reqres.in/api/';
  static final String login = 'login';
  static final String friendList = 'users?page=2';
  static final String termsOfUse =
      'https://generator.lorem-ipsum.info/terms-and-conditions';
}
